package Librarymanagement;

import java.util.Scanner;

public class NormalUser extends User{
	public NormalUser(String name){
		super(name);
		operation=new IO[]{
		new Exit(),
		null,
		null,
		new Search(),
		new Print()
		};
		operation1=new IO2[] {
			null,
			new Borrow(),	
			new Return(),
			null,
			null,
			new Print2(),
		};
	}
	    @Override
		public int menu() {
		System.out.println("..................");
		System.out.println("hello " + name);
		System.out.println("1. 借阅图书");
		System.out.println("2. 归还图书");
		System.out.println("3. 查询书籍");
		System.out.println("4. 查看全部书籍");
		System.out.println("5. 我的借阅");
		System.out.println("0. 退出");
		System.out.println("..................");
		System.out.println("请输入您的选择: ");
		Scanner scanner = new Scanner(System.in);
		int choice = scanner.nextInt();
		return choice;
		}
}
