package Librarymanagement;

import java.util.ArrayList;

public class BookList {	
	public ArrayList<Book> List = new ArrayList();
	public  BookList(){
		
	}
	public int getSize()
	{
		return List.size();
	}
	
	public Book getBook(int i)
	{
		return List.get(i);
	}
	
	public void setBook(int i, Book book)
	{
		List.add(i,book);
	}
	
	public void removeBook(int i)
	{
		List.remove(i);
	}
}
