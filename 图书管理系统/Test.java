package Librarymanagement;

import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		BookList list = new BookList();
		BookList borrowlist = new BookList();
		Book book=new Book("001","1","z",1,1);
		list.setBook(list.getSize(), book);
		Book book1=(new Book("002","2","x",2,2));
		list.setBook(list.getSize(), book1);
		Book book2=(new Book("003","3","h",3,3));
		list.setBook(list.getSize(), book2);
		User user = login();
		 //通过不同的choice和身份调用不同的Operation方法
		while(true){
		    int choice = user.menu();
		    user.doOperation(choice, list,borrowlist);
		}
    }
	public static User login(){
		 Scanner scanner = new Scanner(System.in);
		 System.out.println("请输入你的姓名");
		 String name = scanner.next();
		 System.out.println("请输入你的身份");
		 System.out.println("1.普通用户    2.管理员");
		 int role= scanner.nextInt();
		 if(role==1){
		     return new NormalUser(name);
		 }
		 else{
		     return new Admin(name);
		 }
		 
    }
}
