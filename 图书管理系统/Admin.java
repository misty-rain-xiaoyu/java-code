package Librarymanagement;

import java.util.Scanner;

public class Admin extends User{
	public Admin(String name){
		super(name);
		operation=new IO[]{
		    new Exit(),
		    new Add(),
		    new Delete(),
		    new Search(),
		    new Print()
		};
    }
	@Override
	public int menu() {
		 System.out.println("..................");
		 System.out.println("你好，" + name);
		 System.out.println("1. 增加书籍");
		 System.out.println("2. 删除书籍");
		 System.out.println("3. 查询书籍");
		 System.out.println("4. 查看全部书籍");
		 System.out.println("0. 退出");
		 System.out.println("..................");
		 System.out.println("请输入您的选择: ");
		 Scanner scanner = new Scanner(System.in);
		 int choice = scanner.nextInt();
		 return choice;
	}	
}
