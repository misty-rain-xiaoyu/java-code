package Librarymanagement;

abstract public class User {
	String name;
	protected IO[] operation;
	protected IO2[] operation1;
 	public User(String name){
        this.name=name;
	}
	abstract public int menu();
	public void doOperation(int choice, BookList bookList,BookList borrowList) {
		if(choice!=1&&choice!=5&&choice!=2) {
		    operation[choice].work(bookList);
		}else {
			operation1[choice].work1(bookList,borrowList);
		}
	}
}
