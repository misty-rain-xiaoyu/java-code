package Librarymanagement;

public class Book {

		private String id;
		private String name;
		private String author;
		private Integer number;
		private Integer number1;
		
		public Book(String id, String name, String author, Integer number, Integer number1) {
			super();
			this.id = id;
			this.name = name;
			this.author = author;
			this.number = number;
			this.number1 = number1;
		}
		
		public Book(String id, String name, String author) {
			super();
			this.id = id;
			this.name = name;
			this.author = author;
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public Integer getNumber() {
			return number;
		}
		public void setNumber(Integer number) {
			this.number = number;
		}
		public Integer getNumber1() {
			return number1;
		}
		public void setNumber1(Integer number1) {
			this.number1 = number1;
		}

		@Override
		public String toString() {
			return "书籍 [id：" + id + ", 书名：" + name + ", 作者：" + author + ", 馆藏：" + number + ", 可借："
					+ number1 + "]";
		}
		public String toString1() {
			return "书籍 [id：" + id + ", 书名：" + name + ", 作者：" + author + "]";
		}
}
