package Librarymanagement;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Search<Employee> implements IO{
	@Override
	public void work(BookList bookList) {
		Scanner sc = new Scanner(System.in);
		int flag=0;
		System.out.println("请输入需要查找的书籍的相关信息:");
		String information = sc.next();
		Pattern pattern = Pattern.compile(information);
		   for(int i=0; i < bookList.getSize();i++){
		      Matcher matcher = pattern.matcher(bookList.getBook(i).getName());
		      Matcher matcher1 = pattern.matcher(bookList.getBook(i).getId());
		      Matcher matcher2 = pattern.matcher(bookList.getBook(i).getAuthor());
		      if(matcher.find()||matcher1.find()||matcher2.find()){
		         System.out.println(bookList.getBook(i));
		         flag=1;
		      }
		   }
		   if(flag==0) {
			   System.out.println("查询失败");
		   }
	}
}
